﻿using System;
using System.Diagnostics;
using R8.MIT.API.Protocol.Serialization;
using R8.MIT.AI.BehaviourTree.BehaviourTree.Interfaces;
using R8.MIT.AI.BehaviourTree.Utilities;
using R8.MIT.AI.BehaviourTree.BehaviourTree.Base;
using System.Collections;

namespace R8.MIT.AI.BehaviourTree.BehaviourTree
{
    [Serializable]
	public class BehaviourTree<T> : Prototype<BehaviourTree<T>>, IBehaviourTree<T>
        where T : class
    {
		public event Action<BehaviourTreeNodeState> OnFinished;
		private Action<BehaviourTreeNodeState> _finishCallback;
        
        public BaseTreeNode<T> RootNode;

        public BehaviourTreeNodeState State { get; private set; }
        public T Context { get; private set; }

        private readonly TimeManager _timeManager;
        private readonly PrioritySelector _prioritySelector;

        public BehaviourTree()
		{ 
			State = BehaviourTreeNodeState.Idle;
        }
			
		public BehaviourTree(T context) : this()
		{
			Context = context;
		}

        public IEnumerator Run(T context = null, Action<BehaviourTreeNodeState> finishCallback = null)
        {
            _finishCallback = finishCallback;
            Context = context;

            if (RootNode != null)
            {
                try
                {
                    RootNode.Init(context);
                }
                catch (Exception e)
                {
                    Debug.Write(e.Message + "\r\n" + e.StackTrace);
                    Stop();

                    yield break;
                }
            }
            else
            {
                Stop();

                yield break;
            }

            try
            {
                RootNode.Start();
            }
            catch (Exception e)
            {
                Debug.Write(e.Message + "\r\n" + e.StackTrace);
                Stop();

                yield break;
            }
            
            yield return null;

            while (!RootNode.Completed)
            {
                try
                {
                    State = RootNode.Tick(_timeManager.DeltaTime);
                }
                catch (Exception e)
                {
                    Debug.Write(e.Message + "\r\n" + e.StackTrace);

                    yield break;
                }
                
                yield return null;
            }
            
            Stop();
        }

		public void Stop()
		{
			if (RootNode != null)
			{
				try
				{
					RootNode.Stop();
					State = RootNode.State;
				}
				catch (Exception e)
				{
                    Debug.Write(e.Message + "\r\n" + e.StackTrace);
					State = BehaviourTreeNodeState.Failed;
				}
			}

			OnFinished?.Invoke(State);
			_finishCallback?.Invoke(State);
		}

        public void Dispose()
        {
            _prioritySelector.Dispose();
        }
	}

	[Serializable]
	public class BehaviourTreeLoader<T> where T : class
    {
		public string Path;

		public BehaviourTree<T> Load()
		{
			if (!string.IsNullOrEmpty(Path))
			{
				return ProtocolSerializer.Instance.Deserialize(typeof(BehaviourTree<T>), null) 
                    as BehaviourTree<T>;
			}

			return null;
		}
	}
}

