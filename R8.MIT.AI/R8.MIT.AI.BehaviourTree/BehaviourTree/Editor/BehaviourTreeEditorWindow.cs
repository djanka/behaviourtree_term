﻿using UnityEditor;
using Behaviour;
using CoreEditor;
using System.IO;
using HumanUtilities;

namespace Behaviour
{
    public class BehaviourTreeEditorWindow : EditableData<BehaviourTree> 
	{
        private const string kLastFolderKey = "bteditor_lost_folder_key";
        private const string defaultFolder = "Assets/Resources/GameData";

        [MenuItem("Systems/Behaviour/Behaviour Tree Editor")]
        private static void ShowEditor()
        {
            CustomEditorsFactory.Register<Orientation, OrientationPropertyEditor>();

            DataEditorWindow.Show(new BehaviourTreeEditorWindow());
        }

        public override bool SingleMode { get { return true; } }

        public override string DefaultFolder 
        {
            get 
            {
                var f = EditorPrefs.GetString(kLastFolderKey);
                return string.IsNullOrEmpty(f)
                    ? defaultFolder
                    : f;
            }
        }

        public override string FileExtension { get { return "txt"; } }

        public override void BeforeExport(string path)
        {
            var fpath = Path.GetDirectoryName(path);
            EditorPrefs.SetString(kLastFolderKey, fpath);
        }
	}
}