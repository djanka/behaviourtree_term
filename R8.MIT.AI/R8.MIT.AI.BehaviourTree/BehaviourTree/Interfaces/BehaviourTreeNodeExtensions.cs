﻿namespace R8.MIT.AI.BehaviourTree.BehaviourTree.Interfaces
{
    public static class BehaviourTreeNodeExtensions
    {
        public static bool IsInIdle(this IBehaviourTreeNode node)
        {
            return node.State == BehaviourTreeNodeState.Idle;
        }

        public static bool Succeded(this IBehaviourTreeNode node)
        {
            return node.State == BehaviourTreeNodeState.Succeed;
        }

        public static bool Failed(this IBehaviourTreeNode node)
        {
            return node.State == BehaviourTreeNodeState.Failed;
        }

        public static bool Stopped(this IBehaviourTreeNode node)
        {
            return node.State == BehaviourTreeNodeState.Stopped;
        }
        
        public static bool Running(this IBehaviourTreeNode node)
        {
            return node.State == BehaviourTreeNodeState.Running;
        }

        public static bool Completed(this IBehaviourTreeNode node)
        {
            return node.State == BehaviourTreeNodeState.Failed ||
                   node.State == BehaviourTreeNodeState.Stopped ||
                   node.State == BehaviourTreeNodeState.Succeed;
        }

    }
}
