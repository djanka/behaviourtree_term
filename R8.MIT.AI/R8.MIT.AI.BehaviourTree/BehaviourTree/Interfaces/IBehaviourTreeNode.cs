﻿using Common.Protocol.Declarations;

namespace R8.MIT.AI.BehaviourTree.BehaviourTree.Interfaces
{
    public enum BehaviourTreeNodeState
    {
        Idle,
        Running,
        Failed,
        Succeed,
        Stopped
    }

    public interface IBehaviourTreeNode
    {
        [Protocol("n")] string Name { get; set; }
        [Protocol("w")] int Weight { get; set; }
        
        [ProtocolIgnore] int Priority { get; }
        
        [ProtocolIgnore] BehaviourTreeNodeState State { get; }

        BehaviourTreeNodeState Tick(float deltaTime);
    }

    public interface IBehaviourTreeNode<in T> : IBehaviourTreeNode
    {
        void Init(T context);
        void Start();
        void Stop();
    }

    public interface IBehaviourTreeNodeDecorator<T> : IBehaviourTreeNode<T>
    {
        void Wrap(IBehaviourTreeNode<T> node);
    }

    public interface IBehaviourTreeNodeCompositor<T> : IBehaviourTreeNode<T>
    {
        void AddNode(IBehaviourTreeNode<T> node);
        void ClearNodes();
    }
}
