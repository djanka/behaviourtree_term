﻿using System;
using Behaviour;
using DeliverySystem;

[Serializable]
public class SetDefaultLinkTask : TreeNode 
{
    protected override void OnStart()
    {
        var dc = Game.Controller.City.HarvestDeliverySystem;
        var source = (IDeliverySource<Harvest>) Game.Controller.City.DefaultGrowhouse.Building.Behaviour;
        var destination = (IDeliveryDestination<Harvest>)Game.Controller.City.DefaultSellhouse.Building.Behaviour;

        if (!dc.IsLinked(source) && !dc.IsLinked(destination) && !dc.IsLinked(source, destination))
        {
            dc.AddLinkage(new HarvestLinkage(source, destination));

            SetState(NodeState.Succeed);            
        }
        else
        {
            SetState(NodeState.Failed);
        }        
    }

    public override NodeState Tick(float deltaTime)
    {
        return State;
    }
}
