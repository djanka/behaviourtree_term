﻿using System;

namespace Behaviour
{
    [CoreEditor.CategoryAttribute("Basic")]
    [Serializable]
    public class Noop : TreeNode
    {
        public override NodeState Tick(float dt)
        {
            return SetState(NodeState.Succeed);
        }
    }
}