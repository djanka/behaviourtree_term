﻿using System;
using UnityEngine;
using Core.Data;

namespace Behaviour
{
	[CoreEditor.CategoryAttribute("Basic")]
    [Serializable]
    public class LogTask : TreeNode
	{
		public string text;

		public override NodeState Tick(float deltaTime)
		{
			Debug.Log(text);
			return SetState(NodeState.Succeed);
		}
	}
}
	

