﻿using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json;
using R8.MIT.API.Protocol.Serialization;

namespace R8.MIT.AI.BehaviourTree.BehaviourTree
{
    public static class BehaviourTreePool<T> where T : class
    {
        private static readonly Dictionary<string, BehaviourTree<T>> BehaviourTreesPrototypes = new Dictionary<string, BehaviourTree<T>>();

        public static BehaviourTree<T> CreateBehaviourTree(string path)
        {
            if (!BehaviourTreesPrototypes.ContainsKey(path))
            {
                if (!string.IsNullOrEmpty(path) && File.Exists(path))
                {
                    using (var file = new FileStream(path, FileMode.Open, FileAccess.Read))
                    using (var sr = new StreamReader(file))
                    using (var reader = new JsonTextReader(sr))
                    {
                        var bt = Deserialize<BehaviourTree<T>>(reader);
                        if (bt != null)
                        {
                            BehaviourTreesPrototypes[path] = bt;
                        }
                    }
                }
            }            

            if (BehaviourTreesPrototypes.ContainsKey(path))
            {
                return BehaviourTreesPrototypes[path].DeepCopy();
            }

            Debug.Write("Behaviour Tree " + path + " not found");
            return null;
        }

        private static TData Deserialize<TData>(JsonReader reader) where TData : class 
        {
            var res = ProtocolSerializer.Instance.Deserialize(typeof(TData), Common.Serialization.Wrap(reader)) as TData;
            return res;
        }
    }
}
