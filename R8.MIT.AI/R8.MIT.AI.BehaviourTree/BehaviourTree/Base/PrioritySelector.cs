﻿using R8.MIT.AI.BehaviourTree.BehaviourTree.Interfaces;
using System;
using System.Collections.Generic.Extensions;

namespace R8.MIT.AI.BehaviourTree.BehaviourTree.Base
{
    public interface IPrioritySelector
    {
        IBehaviourTreeNode GetNextNode();
    }

    public class PrioritySelector : IDisposable
    {
        private IReadonlyList<IBehaviourTreeNode> _nodes;

        public PrioritySelector(IReadonlyList<IBehaviourTreeNode> nodes)
        {
            _nodes = nodes;
        }

        public IBehaviourTreeNode GetNextNode()
        {
            IBehaviourTreeNode result = null;

            foreach(var node in _nodes)
            {
                if (result == null || result.Priority < node.Priority)
                {
                    result = node;
                }
            }

            return result;
        }

        public void Dispose()
        {
            _nodes = null;
        }
    }
}
