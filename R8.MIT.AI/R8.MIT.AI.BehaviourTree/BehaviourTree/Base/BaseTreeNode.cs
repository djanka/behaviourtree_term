﻿using Common.Protocol.Declarations;
using R8.MIT.AI.BehaviourTree.BehaviourTree.Interfaces;

namespace R8.MIT.AI.BehaviourTree.BehaviourTree.Base
{
    [Protocol]
    public abstract class BaseTreeNode<T> : IBehaviourTreeNode<T>
    {
        public string Name { get; set; }
        public int Weight { get; set; }

        public int Priority => RecalculatePriority();

        public BehaviourTreeNodeState State { get; protected set; }

        [ProtocolIgnore]
        public bool Completed => State == BehaviourTreeNodeState.Failed || State == BehaviourTreeNodeState.Stopped || State == BehaviourTreeNodeState.Succeed;

	    [ProtocolIgnore]
        public T Context
		{
			get;
			protected set;
		}

        public void Init(T context)
		{
			State = BehaviourTreeNodeState.Idle;
			Context = context;

			OnInit();
        }

        public void Start()
        {
            State = BehaviourTreeNodeState.Running;
            OnStart();
        }

        public void Stop()
		{
			OnStop();

			if (State == BehaviourTreeNodeState.Running)
			{
				State = BehaviourTreeNodeState.Stopped;
			}
        }

        public abstract BehaviourTreeNodeState Tick(float deltaTime);

        public override string ToString()
        {
            return GetType().Name + ": " + Name;
        }

        protected BehaviourTreeNodeState SetState(BehaviourTreeNodeState state)
		{
			State = state;
			return State;
		}

		protected virtual void OnInit() { }
		protected virtual void OnStart() { }
		protected virtual void OnStop() { }

        protected abstract int RecalculatePriority();
	}
}

