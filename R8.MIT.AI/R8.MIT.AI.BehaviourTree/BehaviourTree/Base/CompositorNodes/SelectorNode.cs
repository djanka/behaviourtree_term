﻿using R8.MIT.AI.BehaviourTree.BehaviourTree.Interfaces;

namespace R8.MIT.AI.BehaviourTree.BehaviourTree.Base
{
    public class SelectorNode<T> : BaseCompositorNode<T>
    {
        public override BehaviourTreeNodeState Tick(float deltaTime)
        {
            foreach (var child in Children)
            {
                if (child.IsInIdle())
                    child.Start();

                if (child.Completed())
                    child.Tick(deltaTime);

                if (child.Completed())
                {
                    if (child.Succeded())
                        return SetState(BehaviourTreeNodeState.Succeed);

                    continue;
                }
                else
                    return SetState(BehaviourTreeNodeState.Running);
            }

            return SetState(BehaviourTreeNodeState.Failed);
        }
    }
}
