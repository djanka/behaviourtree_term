﻿using R8.MIT.AI.BehaviourTree.BehaviourTree.Interfaces;

namespace R8.MIT.AI.BehaviourTree.BehaviourTree.Base.CompositorNodes
{
    public class SequenceNode<T> : BaseCompositorNode<T>
    {
        public override BehaviourTreeNodeState Tick(float deltaTime)
        {
            foreach (var child in Children)
            {
                if (child.State == BehaviourTreeNodeState.Idle)
                    child.Start();

                if (!child.Completed())
                    child.Tick(deltaTime);

                if (child.Completed())
                {
                    if (child.Succeded())
                        continue;

                    return SetState(child.State);
                }

                return SetState(BehaviourTreeNodeState.Running);
            }

            return BehaviourTreeNodeState.Succeed;
        }
    }
}
