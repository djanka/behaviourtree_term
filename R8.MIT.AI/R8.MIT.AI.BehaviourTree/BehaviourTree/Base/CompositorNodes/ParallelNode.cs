﻿using Common.Protocol.Declarations;
using R8.MIT.AI.BehaviourTree.BehaviourTree.Base;
using R8.MIT.AI.BehaviourTree.BehaviourTree.Interfaces;

namespace BehaviourTree.Base
{
    public class ParallelNode<T> : BaseCompositorNode<T>
    {
        [Protocol("f")] public int NumRequiredToFail;
        [Protocol("s")] public int NumRequiredToSucceed;

        public override BehaviourTreeNodeState Tick(float deltaTime)
        {
            var numChildrenSuceeded = 0;
            var numChildrenFailed = 0;
            
            foreach (var child in Children)
            {
                if (child.IsInIdle())
                {
                    child.Start();
                }

                if (!child.Completed())
                    child.Tick(deltaTime);

                var childStatus = child.Tick(deltaTime);
                switch (childStatus)
                {
                    case BehaviourTreeNodeState.Succeed: ++numChildrenSuceeded; break;
                    case BehaviourTreeNodeState.Failed: ++numChildrenFailed; break;
                }
            }

            if (NumRequiredToSucceed > 0 && numChildrenSuceeded >= NumRequiredToSucceed)
            {
                return BehaviourTreeNodeState.Succeed;
            }

            if (NumRequiredToFail > 0 && numChildrenFailed >= NumRequiredToFail)
            {
                return BehaviourTreeNodeState.Failed;
            }

            return BehaviourTreeNodeState.Running;
        }
    }
}
