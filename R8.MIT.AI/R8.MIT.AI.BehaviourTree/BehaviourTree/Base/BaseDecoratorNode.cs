﻿using Common.Protocol.Declarations;
using R8.MIT.AI.BehaviourTree.BehaviourTree.Interfaces;

namespace R8.MIT.AI.BehaviourTree.BehaviourTree.Base
{
    [Protocol]
    public abstract class BaseDecoratorNode<T> : BaseTreeNode<T>, IBehaviourTreeNodeDecorator<T>
    {
        [Protocol("c")] public IBehaviourTreeNode<T> Child { get; set; }

        public void Wrap(IBehaviourTreeNode<T> node)
        {
            Child?.Stop();
            Child = node;
            State = BehaviourTreeNodeState.Stopped;
        }

        protected override void OnInit()
        {
            Child.Init(Context);
        }

        protected override void OnStop()
        {
            Child.Stop();
        }

        protected override void OnStart()
        {
            Child.Start();
        }

        protected override int RecalculatePriority()
        {
            return Child.Priority;
        }
    }
}
