﻿using R8.MIT.AI.BehaviourTree.BehaviourTree.Base;
using R8.MIT.AI.BehaviourTree.BehaviourTree.Interfaces;
using System;

namespace BehaviourTree.Base
{
    public class InverterNode<T> : BaseDecoratorNode<T>
    {
        public override BehaviourTreeNodeState Tick(float deltaTime)
        {
            if (Child == null)
            {
                throw new ApplicationException("InverterNode must have a child node!");
            }

            var result = Child.Tick(deltaTime);
            if (result == BehaviourTreeNodeState.Failed)
            {
                return BehaviourTreeNodeState.Succeed;
            }
            else if (result == BehaviourTreeNodeState.Succeed)
            {
                return BehaviourTreeNodeState.Failed;
            }
            else
            {
                return result;
            }
        }
    }
}
