﻿using System.Collections.Generic.Extensions;
using Common.Protocol.Declarations;
using R8.MIT.AI.BehaviourTree.BehaviourTree.Interfaces;

namespace R8.MIT.AI.BehaviourTree.BehaviourTree.Base
{
    public abstract class BaseCompositorNode<T> : BaseTreeNode<T>, IBehaviourTreeNodeCompositor<T>
    {
        [Protocol("c")] public EList<IBehaviourTreeNode<T>, IBehaviourTreeNode> Children;

        private PrioritySelector _prioritySelector;

        protected override void OnInit()
        {
            foreach (var child in Children)
            {
                child.Init(Context);
            }
        }

        protected override void OnStart()
        {
            _prioritySelector = new PrioritySelector(Children.AsInterfaceReadonly());
             
            foreach (var child in Children)
            {
                child.Start();
            }
        }

        protected override void OnStop()
        {
            foreach (var child in Children)
            {
                child.Stop();
            }
        }

        public void AddNode(IBehaviourTreeNode<T> node)
        {
            Children.Add(node);

            if (State == BehaviourTreeNodeState.Running)
                node.Start();
        }

        public void ClearNodes()
        {
            foreach (var node in Children)
            {
                node.Stop();
            }

            Children.Clear();
        }

        protected override int RecalculatePriority()
        {
            return _prioritySelector.GetNextNode().Priority;
        }
    }
}
