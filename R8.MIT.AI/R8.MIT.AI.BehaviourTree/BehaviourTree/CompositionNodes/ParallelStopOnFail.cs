﻿using System;

namespace Behaviour
{
    /// <summary>
	/// Executes all children at the same time
	/// Succeed when all children finished
	/// Fails if child failed
	/// </summary>
    [CoreEditor.CategoryAttribute("Compositors")]
    [Serializable]
    public class ParallelStopOnFail : Compositor
    {
        public override NodeState Tick(float deltaTime)
        {
            var completed = 0;
            var failed = 0;

            for (int i = 0; i < children.Count; i++)
            {
                TreeNode child = children[i];
                if (child.Completed)
                {
                    completed++;
                    if (child.State == NodeState.Failed)
                    {
                        failed++;
                        break;                        
                    }

                    continue;
                }
                if (child.State == NodeState.Idle)
                {
                    child.Start();
                }
                child.Tick(deltaTime);
                if (child.Completed)
                {
                    completed++;
                    if (child.State == NodeState.Failed)
                    {
                        failed++;
                        break;
                    }
                }
            }

            if (failed > 0)
            {
                SetState(NodeState.Failed);
            }
            else
            {
                SetState(completed >= children.Count ? NodeState.Succeed : NodeState.Running);
            }
            return State;
        }
    }
}
