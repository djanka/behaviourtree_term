﻿using System;
using System.Collections.Generic;

namespace Behaviour
{
	/// <summary>
	/// Starts next child when previous Fails
	/// Finish when one of children succeed
	/// Fails if all of the children Failed
	/// </summary>
	[CoreEditor.CategoryAttribute("Compositors")]
    [Serializable]
    public class Selector : Compositor
	{
		public override NodeState Tick(float deltaTime)
		{
            for (int i = 0; i < children.Count; i++)
            {
                TreeNode child = children[i];

                if (child.State == NodeState.Idle)
                {
                    child.Start();
                }

                if (!child.Completed)
                {
                    child.Tick(deltaTime);
                }

                if (child.Completed)
                {
                    if (child.State == NodeState.Succeed)
                    {
                        return SetState(NodeState.Succeed);
                    }
                    else
                    {
                        continue;
                    }
                } 
                else
                {
                    return SetState(NodeState.Running);
                }
            }

			//All nodes failed or stopped
			return SetState(NodeState.Failed);
		}
	}
}

