﻿
using System;

namespace Behaviour
{
	/// <summary>
	/// Executes children one by one, starting next when previous Succed
	/// Fails when child Failed
	/// </summary>
	[CoreEditor.CategoryAttribute("Compositors")]
    [Serializable]
    public class Sequence : Compositor
	{
		public override NodeState Tick(float deltaTime)
		{
			for (int i = 0; i < children.Count; i++)
            {
                TreeNode child = children[i];

                if (child.State == NodeState.Idle)
                    child.Start();
                
                if (!child.Completed)
                    child.Tick(deltaTime);

                if (child.Completed)
                {
                    if (child.State == NodeState.Succeed)
                        continue;
                    
                    return SetState(child.State);
                }

                return SetState(NodeState.Running);
            }

			return SetState(NodeState.Succeed);
		}
	}
}

