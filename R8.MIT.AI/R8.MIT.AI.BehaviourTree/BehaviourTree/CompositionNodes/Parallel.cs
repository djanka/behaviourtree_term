﻿using System;
using System.Collections.Generic;

namespace Behaviour
{
	/// <summary>
	/// Executes all children at the same time
	/// Succeed when all children finished
	/// Never Fail
	/// </summary>
	[CoreEditor.CategoryAttribute("Compositors")]
    [Serializable]
    public class Parallel : Compositor
	{
		public override NodeState Tick(float deltaTime)
		{
			var completed = 0;

			for (int i = 0; i < children.Count; i++)
            {
                TreeNode child = children[i];
                if (child.Completed)
                {
                    completed++;
                    continue;
                }
                if (child.State == NodeState.Idle)
                {
                    child.Start();
                }
                child.Tick(deltaTime);
                if (child.Completed)
                {
                    completed++;
                }
            }

			return SetState(completed >= children.Count ? NodeState.Succeed : NodeState.Running);
		}
	}
}

