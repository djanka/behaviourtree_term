﻿using System;
using Behaviour;
using UnityEngine;

namespace Behaviour
{
	[CoreEditor.CategoryAttribute("Basic")]
    [Serializable]
    public class WaitNode : TreeNode
	{
		public float time;
        public float Offset;

		private float _timePassed = 0f;
        private float _randomOffset = 0f;

		public WaitNode()
		{ }
        public WaitNode(float time)
		{
			this.time = time;
		}

		protected override void OnStart()
		{
			_timePassed = 0f; 
            if (Offset != 0)
            { 
                _randomOffset = UnityEngine.Random.Range(0f, Offset);
            }
        }

		public override NodeState Tick(float deltaTime)
		{
		    _timePassed += deltaTime;
            if(_timePassed >= time + _randomOffset)
			{
				return SetState(NodeState.Succeed);
			}
			return SetState(NodeState.Running);
		}
	}
}

