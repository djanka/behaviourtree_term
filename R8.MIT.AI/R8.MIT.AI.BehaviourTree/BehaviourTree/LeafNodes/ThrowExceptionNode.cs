﻿using System;
using UnityEngine;
using System.Collections;

namespace Behaviour
{
	[CoreEditor.CategoryAttribute("Others")]
    [Serializable]
    public class ThrowExceptionNode : TreeNode
	{
		public bool ThrowOnInit;
		public bool ThrowOnStart;
		public bool ThrowOnTick;
		public bool ThrowOnStop;

		protected override void OnInit()
		{
			if(ThrowOnInit)
				throw new System.Exception ("BT OnInit Exception.");
		}
		protected override void OnStart()
		{
			if(ThrowOnStart)
				throw new System.Exception ("BT OnStart Exception.");
		}
		public override NodeState Tick(float deltaTime)
		{
			if(ThrowOnTick)
				throw new System.Exception ("BT OnTick Exception.");
			
			return SetState(NodeState.Succeed);
		}
		protected override void OnStop()
		{
			if(ThrowOnStop)
				throw new System.Exception ("BT OnStop Exception.");
		}
	}
}