﻿using System;
using System.Collections;
using Core.Data;

namespace Behaviour
{
	[CoreEditor.CategoryAttribute("Others")]
    [Serializable]
    public class SetContextValue : TreeNode
	{
		public string key;
		public ContextVar NewValueVar;

		public override NodeState Tick(float deltaTime)
		{
			if(Context == null)
			{
				return SetState(NodeState.Failed);
			}

			Context.Set(key, NewValueVar.Evaluate());

			return SetState(NodeState.Succeed);
		}
	}
}

