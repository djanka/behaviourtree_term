﻿using System;
using Behaviour;
using UnityEngine;

namespace Behaviour
{
    [CoreEditor.CategoryAttribute("Human Behaviour")]
    [Serializable]
    public class WaitWalkFinish : HumanTask
	{
        public WaitWalkFinish()
		{ }
    
		public override NodeState Tick(float deltaTime)
		{
	
            if(!_target.HumanMovementComponent.IsInProgress)
			{  
				return SetState(NodeState.Succeed);
			}
			return SetState(NodeState.Running);
		}
	}
}

