﻿using System;
using Core.Data;

namespace Behaviour
{
    [CoreEditor.CategoryAttribute("Others")]
    [Serializable]
    public class CheckBoolContextValue : TreeNode
    {
        public string key;
        public bool value;

        public override NodeState Tick(float deltaTime)
        {
            if (Context == null)
            {
                return SetState(NodeState.Failed);
            }
            var val = Context.Evaluate(key);
            if (val == null)
            {
                return SetState(NodeState.Failed);
            }

            if (val is bool)
            {
                return SetState(value.Equals(val) ? NodeState.Succeed : NodeState.Failed);
            }

            return NodeState.Failed;
        }
    }
}
