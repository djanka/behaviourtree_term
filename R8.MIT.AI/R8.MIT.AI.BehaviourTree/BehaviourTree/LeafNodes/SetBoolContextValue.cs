﻿using System;

namespace Behaviour
{
    [CoreEditor.CategoryAttribute("Others")]
    [Serializable]
    public class SetBoolContextValue : TreeNode
    {
        public string key;
        public bool value;

        public override NodeState Tick(float deltaTime)
        {
            if (Context == null)
            {
                return SetState(NodeState.Failed);
            }

            Context.Set(key, value);

            return SetState(NodeState.Succeed);
        }
    }
}
