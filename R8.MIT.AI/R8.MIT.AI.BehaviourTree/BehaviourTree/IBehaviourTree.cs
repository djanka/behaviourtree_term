﻿using System;
using R8.MIT.AI.BehaviourTree.BehaviourTree.Interfaces;
using System.Collections;

namespace R8.MIT.AI.BehaviourTree.BehaviourTree
{
    public interface IBehaviourTree<T> : IDisposable
    {
        IEnumerator Run(T context, Action<BehaviourTreeNodeState> finishCallback = null);
        void Stop();
    }
}
