﻿using System;

namespace Behaviour
{
	[CoreEditor.CategoryAttribute("Decorators")]
    [Serializable]
    public class Succeder : Decorator
	{
		public override NodeState Tick(float deltaTime)
		{
			if(!child.Completed)
			{
				child.Tick(deltaTime);
			}
			if(child.State == NodeState.Running)
			{
				return SetState(NodeState.Running);
			}
			return SetState(NodeState.Succeed);
		}
	}
}

