﻿using System;

namespace Behaviour
{
	[CoreEditor.CategoryAttribute("Decorators")]
    [Serializable]
    public class Inverter : Decorator
	{
		public override NodeState Tick(float deltaTime)
		{
			if(!child.Completed)
			{
				child.Tick(deltaTime);
			}
			if(child.State == NodeState.Running)
			{
				return SetState(NodeState.Running);
			}
			if(child.State == NodeState.Stopped)
			{
				return SetState(NodeState.Stopped);
			} 
			else
			{
				return SetState(child.State == NodeState.Succeed ? NodeState.Failed : NodeState.Succeed);
			}
		}
	}
}

