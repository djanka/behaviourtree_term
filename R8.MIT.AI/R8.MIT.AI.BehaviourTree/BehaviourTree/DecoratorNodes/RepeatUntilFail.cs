﻿using System;

namespace Behaviour
{
	[CoreEditor.CategoryAttribute("Decorators")]
    [Serializable]
    public class RepeatUntilFail : Decorator
	{

	    public override NodeState Tick(float deltaTime)
		{
			if(!child.Completed)
			{
				child.Tick(deltaTime);
			}

			if(child.Completed)
			{
				if(child.State == NodeState.Failed || child.State == NodeState.Stopped)
				{
					return SetState(NodeState.Succeed);
				}
				else
				{		
                    child.Init(Context);
					child.Start();
				}
			}
			return SetState(NodeState.Running);
		}
	}
}

