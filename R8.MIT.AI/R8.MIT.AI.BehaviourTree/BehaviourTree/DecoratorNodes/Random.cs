﻿using System;
using UnityEngine;

namespace Behaviour
{
    [CoreEditor.Category("Decorators")]
    [Serializable]
    public class RandomExecutor : Decorator
    {
        [Range(0f,1f)]
        public float probability = 0.5f;

        private bool _execute = false;

        protected override void OnStart()
        {
            _execute = false;

            if (UnityEngine.Random.value < probability)
            {
                _execute = true;
                base.OnStart();
            }
        }

        public override NodeState Tick(float deltaTime)
        {
            if (!_execute)
            {
                return SetState(NodeState.Failed);
            }

            if (!child.Completed)
            {
                child.Tick(deltaTime);
            }
            if (child.State == NodeState.Running)
            {
                return SetState(NodeState.Running);
            }
            return SetState(child.State);
        }
    }
}
