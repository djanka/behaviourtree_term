﻿using System;

namespace Behaviour
{
	[CoreEditor.CategoryAttribute("Decorators")]
    [Serializable]
    public class Repeater : Decorator
	{
	    public override NodeState Tick(float deltaTime)
		{
			if(child.Completed)
			{	
                child.Init(Context);
                child.Start();
			}
			child.Tick(deltaTime);
			return SetState(NodeState.Running);
		}
	}
}

