﻿using System;
using Common.Normalization;
using R8.MIT.AI.BehaviourTree.Utilities.Scheduler;

namespace R8.MIT.AI.BehaviourTree.Utilities
{
    public class TimeManager
    {
        private readonly IScheduler _scheduler = new Scheduler.Scheduler();

        public TimeManager(DateTime currenTime, Action action)
        {
            StartTime = currenTime.ToUnixSeconds();
            DeltaTime = 0;

            _scheduler.Schedule(action, 1f, -1);
        }

        public float DeltaTime { get; private set; }
        public long StartTime { get; private set; }

        private void Update()
        {
            
        }
    }
}
