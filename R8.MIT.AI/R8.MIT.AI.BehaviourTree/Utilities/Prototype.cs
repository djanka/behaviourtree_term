﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace R8.MIT.AI.BehaviourTree.Utilities
{
    [Serializable]
    public abstract class Prototype<T>
    {
        public T Clone()
        {
            return (T)this.MemberwiseClone();
        }

        public T DeepCopy()
        {
            var stream = new MemoryStream();
            var formatter = new BinaryFormatter();
            formatter.Serialize(stream, this);
            stream.Seek(0, SeekOrigin.Begin);
            var copy = (T)formatter.Deserialize(stream);
            stream.Close();
            return copy;
        }
    }
}
