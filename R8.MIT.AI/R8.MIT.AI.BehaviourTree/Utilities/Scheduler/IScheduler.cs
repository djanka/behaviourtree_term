﻿using System;

namespace R8.MIT.AI.BehaviourTree.Utilities.Scheduler
{
    public interface IScheduler
    {
        ISchedulerHandle Schedule(Action action, float seconds = 0f, int repeat = 1);
        void Unschedule(ISchedulerHandle handle);
    }

    public interface ISchedulerHandle
    {
        int Id { get; }
        float OriginalDuration { get; }

        int RepeatsLeft { get; }
        bool IsInfiniteRepeated { get; }
        float SecondsLeft { get; } // for current loop

        bool IsPaused { get; set; }
        
        event Action<float> Ticked;
        event Action Finished;
    }
}
