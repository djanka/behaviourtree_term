﻿using System;

namespace R8.MIT.AI.BehaviourTree.Utilities.Scheduler
{
    public class SchedulerHandle : ISchedulerHandle, IDisposable
    {
        public int Id { get; }
        public float OriginalDuration { get; }

        public event Action<float> Ticked;
        public event Action Finished;

        public bool IsPaused { get; set; }
        public bool Dead { get; private set; }

        public bool IsInfiniteRepeated => _repeatTimes < 0;

        public int RepeatsLeft => _repeatTimes;
        public float SecondsLeft => _secondsLeft;

        private bool IsValid => _action != null;

        private Action _action;
        private float _secondsLeft;
        private int _repeatTimes;

        public SchedulerHandle(int id, float seconds, int repeatTimes)
        {
            Id = id;
            OriginalDuration = seconds;
            _secondsLeft = seconds;
            _repeatTimes = repeatTimes;
        }

        public SchedulerHandle SetAction(Action action)
        {
            _action = action;
            return this;
        }

        public bool IsAction(Action action)
        {
            return action != null && _action == action;
        }

        public void Invoke(float deltaTime)
        {
            Dead = InvokeImpl(deltaTime);
        }

        public void Dispose()
        {
            Finish();
        }

        private bool InvokeImpl(float deltaTime)
        {
            if (!IsValid)
                return true;

            if (IsPaused)
                return false;

            Ticked?.Invoke(deltaTime);

            if (_secondsLeft <= 0f)
            {
                try
                {
                    _action?.Invoke();
                }
                catch (Exception e)
                {
                    Common.Logging.Error.Log("Unhandled exception in action, data = " + ToString());
                    Common.Logging.Error.Log(e);
                }

                if (_repeatTimes > 0)
                    --_repeatTimes;

                if (_repeatTimes != 0)
                    _secondsLeft = OriginalDuration;
            }

            _secondsLeft -= deltaTime;

            if (_repeatTimes != 0)
                return false;

            Finish();

            return true;
        }

        private void Finish()
        {
            _secondsLeft = 0f;
            _repeatTimes = 0;

            Finished?.Invoke();

            IsPaused = false;
            Finished = null;
            Ticked = null;
            _action = null;
        }

        public override string ToString()
        {
            if (_action == null)
                return "<empty>";

            var target = _action.Target;
            var targetStr = target == null ? "<no_target>" : target.ToString();

            return string.Format("[ActionData: Sender = {0}, Method = {1}]", targetStr, _action.Method);
        }
    }
}
