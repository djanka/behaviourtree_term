﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;

namespace R8.MIT.AI.BehaviourTree.Utilities.Scheduler
{
    public class Scheduler : IScheduler, IDisposable
    {
        private Timer _timer;

        private readonly List<SchedulerHandle> _actions = new List<SchedulerHandle>();
        private List<SchedulerHandle> _actionsCache;

        private int _actionsSeed;

        public Scheduler()
        {
            _timer = new Timer(0.016f);
            _timer.Elapsed += (sender, e) => Update();
            _timer.Start();
        }

        public void Run(IEnumerator coroutine)
        {
            StartCoroutine(coroutine);
        }

        public void Stop(IEnumerator coroutine)
        {
            StopCoroutine(coroutine);
        }

        public ISchedulerHandle Schedule(Action action, float seconds = 0f, int repeat = 1)
        {
            return action == null
                ? null
                : CreateAction(seconds, repeat).SetAction(action);
        }

        public void Unschedule(Action action)
        {
            UnscheduleAll(a => a.IsAction(action));
        }

        public void Unschedule(int id)
        {
            UnscheduleAll(a => a.Id == id);
        }

        public void Unschedule(ISchedulerHandle handle)
        {
            UnscheduleAll(a => ReferenceEquals(a, handle));
            DisposeTimer();
        }
        
        public void Dispose()
        {
            DisposeTimer();
        }

        private void DisposeTimer()
        {
            _timer?.Dispose();
            _timer = null;
        }
        
        private void UnscheduleAll(Predicate<SchedulerHandle> predicate)
        {
            for (int i = 0; i < _actions.Count;)
            {
                var a = _actions[i];
                if (predicate(a))
                {
                    _actions.RemoveAt(i);
                    a.Dispose();
                }
                else
                    ++i;
            }
        }

        private SchedulerHandle CreateAction(float seconds, int repeat)
        {
            if (repeat == 0)
                repeat = 1;

            var d = new SchedulerHandle(++_actionsSeed, seconds, repeat);
            _actions.Add(d);
            return d;
        }

        protected void Update()
        {
            UpdateTimers(0.016f);
        }

        private void UpdateTimers(float deltaTime)
        {
            if (_actions.Count == 0)
                return;

            if (_actionsCache == null)
                _actionsCache = new List<SchedulerHandle>();

            _actionsCache.AddRange(_actions);

            for (int i = 0; i < _actionsCache.Count; ++i)
            {
                var a = _actionsCache[i];

                a.Invoke(deltaTime);
            }

            _actionsCache.Clear();

            for (int i = 0; i < _actions.Count;)
            {
                if (_actions[i].Dead)
                    _actions.RemoveAt(i);
                else
                    ++i;
            }
        }

        private void StartCoroutine(IEnumerator coroutine)
        {
            
        }

        private void StopCoroutine(IEnumerator coroutine)
        {

        }
    }
}
