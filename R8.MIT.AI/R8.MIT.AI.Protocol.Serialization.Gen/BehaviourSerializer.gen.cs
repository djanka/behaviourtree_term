using System;
using System.Collections.Generic;
using Common.SerializationWrappers;
using Common.Protocol.Serialization.Base;

namespace R8.MIT.AI.Protocol.Serialization.Gen
{
    public static class BehaviourSerializer
    {
        [ThreadStatic]
        private static BehaviourSerializerGen _instance;

        public static ISerializerGeneralized Instance { get { return _instance ?? (_instance = new BehaviourSerializerGen()); } }
    }
}
