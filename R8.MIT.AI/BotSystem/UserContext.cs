﻿using R8.MIT.Game.Abstractions;
using R8.MIT.Game.Models.States;
using R8.MIT.Game.Models.Static;

namespace R8.MIT.AI.BotSystem
{
    public class UserContext : IUserContext
    {
        public StaticDataContext StaticData => _staticData;
        public UserState User => _userState;

        private readonly StaticDataContext _staticData;
        private readonly UserState _userState;

        public UserContext(UserState state, StaticDataContext staticData)
        {
            _staticData = staticData;
            _userState = state;
        }
    }
}
