﻿using System.Collections.Generic;

namespace R8.MIT.AI.BotSystem
{
    public class BotSystem
    {
        private readonly List<BotController> _aiCollection = new List<BotController>();

        public BotSystem(int clientsCount)
        {
        }

        public void Start()
        {
            foreach (var bot in _aiCollection)
            {
                bot.Start();
            }
        }

        public void Stop()
        {
            StopClients();
        }

        private void StopClients()
        {
            foreach (var bot in _aiCollection)
            {
                bot.Stop();
            }
        }
    }
}
