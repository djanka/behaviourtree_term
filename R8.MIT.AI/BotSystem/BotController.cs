﻿using System;
using R8.MIT.AI.BehaviourTree.BehaviourTree;
using R8.MIT.Game.Abstractions;

namespace R8.MIT.AI.BotSystem
{
    public class BotController : IDisposable
    {
        private readonly IUserContext _context;
        private readonly IBehaviourTree<IUserContext> _behaviour;

        private const string BehaviourTreePath = "bt";

        public static BotController Create(IUserContext context)
        {
            return new BotController(context);
        }

        private BotController(IUserContext context)
        {
            _context = context;
            
            _behaviour = BehaviourTreePool<IUserContext>.CreateBehaviourTree(BehaviourTreePath);
        }

        public void Start()
        {
            _behaviour.Run(_context);
        }

        public void Stop()
        {
            _behaviour.Stop();
        }

        public void Dispose()
        {
            _behaviour.Dispose();
        }
    }
}
