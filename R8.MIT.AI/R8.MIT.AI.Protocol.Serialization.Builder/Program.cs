﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Common.Protocol.Serialization.Generation;
using R8.MIT.AI.BehaviourTree;

namespace R8.MIT.AI.Protocol.Serialization.Builder
{
    internal class Program
    {
        private static readonly Assembly[] Targets =
        {
            typeof(BehaviourSerializationAnchor).Assembly
        };

        static void Main(string[] args)
        {
            const string className = "BehaviourSerializer";
            const string filesPrefix = className;
            const string projectName = "R8.MIT.AI.Protocol.Serialization.Gen";
            const string @namespace = "R8.MIT.AI.Protocol.Serialization.Gen";

            var path = "../../../" + projectName + "/";

            SerializationGenerator.Generate(path,
                className: className,
                @namespace: @namespace,
                filesPrefix: filesPrefix,
                targetAssemblies: Targets,
                test: false);
        }
    }
}
